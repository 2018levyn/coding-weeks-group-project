#!/usr/bin/env python3

import feedparser  # http://blog.topspeedsnail.com/archives/8156
import sys
import shelve
import os
from weibo import Client  # https://github.com/lxyu/weibo
import socket
import tweepy  # http://blog.topspeedsnail.com/archives/3380

rss_uri = "http://blog.topspeedsnail.com/feed"
entries_n = 7  # 获得文章数

# 保存发送过的文章
cache_max = 100
if not os.path.exists('cache.db'):
    db = shelve.open('cache.db', writeback=True)
    db['article_list'] = []
else:
    db = shelve.open('cache.db', writeback=True)


# 判断是否联网
def is_connected():
    try:
        REMOTE_SERVER = "www.baidu.com"
        host = socket.gethostbyname(REMOTE_SERVER)
        s = socket.create_connection((host, 80), 2)
        return True
    except:
        pass
    return False


if is_connected() is False:
    print("检查网络连接")
    sys, exit(0)

# 推特认证
consumer_key = 'qdte0pMyPvk6Puxxxxxxxx'
consumer_secret = 'lQ4xTLhBmv4KaYlusBH04jppeo9Nfm6WudFxxxxxxxx'
access_token = '909480547-Soz5w3N22gOkITEXUsvrlU41HkxsC5xxxxxxx'
access_token_secret = '3j08IYs1pwPNnchmlcNK7wagGXuJcBtkjcsxxxxxxxx'
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
auth.secure = True
api = tweepy.API(auth)

# 获得feed
feed = feedparser.parse(rss_uri)

if feed['status'] != 200:
    print("连接异常")
    sys.exit(0)

entries = feed['entries'][0:entries_n]
# entries.reverse()

# 发送
for entry in entries:
    if entry['id'] not in db['article_list']:
        # 构造要发送的内容
        """
        # 带hashtag的推文
        if 'tags' in entry:
            print('有hastag')
        else:
            print('没有hastag')
        """
        # 只发送标题和链接
        send_content = entry['title'] + ' - ' + entry['id']
        # 使用bitly短链接服务缩小链接所占字数
        # short_link = bitly_api.Connection(access_token='cb3e9e1ce06f2b29852f0f83702e9xxxxxxxxx')
        # short_url = short_link.shorten(entry['id'])
        print(send_content)
        try:
            weibo_client.post('statuses/update', status=send_content)
            api.update_status(status=send_content)
        except Exception as e:
            print(str(e))

        # 如果达到缓存上限,删除第一个元素,然后append
        if len(db['article_list']) == cache_max:
            del db['article_list'][0]
        db['article_list'].append(entry['id'])

db.close()
#