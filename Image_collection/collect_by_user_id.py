import twitter_connection
import os
import urllib.request


def create_folder(user_name):
    '''
    Creates a folder to store the images
    :return: the path to the folder
    '''
    folder = "Images_posted_by_{}".format(user_name)
    if not os.path.exists(folder):
        os.makedirs(folder)

    path = os.getcwd() + "\{}".format(folder)
    return path

def collect_by_user(user_name):
    '''
    Collects pictures tweeted by a certain user
    :param user_id: string
    :return: pictures
    '''
    connexion = twitter_connection.twitter_setup()
    statuses = connexion.user_timeline(screen_name = user_name, count = 200)

    path = create_folder(user_name)

    k = 0
    for status in statuses:
        if "media" in status.entities.keys():
            for elem in status.entities['media']:
                if "media_url" in elem.keys():
                    if ".jpg" in elem['media_url']:
                        url = elem['media_url']
                        print(k)
                        urllib.request.urlretrieve(url,"Image_{}_{}.jpg".format(user_name,k))
                        k+=1



    return statuses

collect_by_user("Trump")

